import datetime

from bson.json_util import dumps
from pymongo import MongoClient
from pymongo import database


def readyJSONObject(json_arg: dict):
    # Converting Creation Date and Time to UTC for Mongo supported format
    # Uses POSIX timestamps YYYY-MM-DD(T)HH:MM:SS:ss(Z) - where (T) and (Z) are just T and Z
    createDateTime: datetime.datetime = datetime.datetime.strptime(
        str(json_arg.get("CreationDate") + ' ' + json_arg.get("CreationTime"))[:-4], "%Y-%m-%d %H:%M:%S")
    completeDateTime: datetime.datetime = datetime.datetime.strptime(
        str(json_arg.get("CompletionDate") + ' ' + json_arg.get("CompletionTime") + ":00"), "%Y-%m-%d %H:%M:%S")
    json_arg.update(CreationDate=createDateTime.date().isoformat())
    json_arg.update(CreationTime=createDateTime.time().isoformat())
    json_arg.update(CompletionDate=completeDateTime.date().isoformat())
    json_arg.update(CompletionTime=completeDateTime.time().isoformat())

    if json_arg.pop("_id", False):
        print("Removed _id")
    return json_arg


class dB:
    """
    The main Database class that interacts with MongoDB for RemindYOU
    Initiates connection with MongoDB and queries Mongo collections
    """
    connection: MongoClient()  # MongoClient object used to establish connection to database.
    dbase = database.Database  # Database object used to connect to MongoDB.
    rCollection = database.Collection  # Collection object used to talk with reminders collection.
    connectionTest: bool = False  # Bool that keeps track of initial connection
    uCollection = database.Collection

    def __init__(self):
        """
        dB class init bloc
        Checks if there is already a current connection to MongoDB.
        If not initializes connection.
        """
        if not self.checkConnect():
            self.connection = self.getConnection()
            self.dbase = self.connection.get_database("Reminding")
            self.rCollection = self.dbase.get_collection("reminder")
            self.uCollection = self.dbase.get_collection("users")
            print("Connected via INIT block")
            print("Database: " + self.dbase.name)
            print("Collection: " + self.rCollection.full_name)
            self.connectionTest = True

    @staticmethod
    def getConnection():
        """
        Connection static method.
        Reads from config file and returns the driver string to connect to the database.
        In file called configFile. Place driver string after as so -
        mongo driver string: <driver string here>
        """
        config = open("configFile", "r")
        mongoDriver = " "
        for i in config:
            if i[0:20] == "mongo driver string:":
                mongoDriver = i[21:]
                mongoDriver = mongoDriver.rstrip('\n')
        config.close()
        return MongoClient(mongoDriver)

    def addReminder(self, reminder_args: dict):
        """
        TODO: Need to tag user ID to the reminder OwnerID / UserIDs
        IN USE - adds a reminder to MongoDB Reminding.reminde
        Params -
        * 'json_args' (dict): This is the JSON object that will be added to the database
        -end
        """
        print("addReminder")
        reminder = {}
        if self.checkConnect():
            # Getting up to date ReminderID from Mongo
            if self.checkReminderDBStructure(reminder_args):
                if reminder_args["ReminderID"] == 0:
                    index = self.rCollection.count_documents({})
                    reminder_args.update(ReminderID=index)
                    self.rCollection.insert_one(reminder_args)
                    objectID: str = str(reminder_args.get("_id"))
                    reminder_args.update(_id=objectID)
                else:
                    return self.updateReminder(reminder_args)
            else:
                return {"Error": "AddReminder"}
            return reminder_args
        else:
            return {"Error": "addReminderError"}

    def checkReminderDBStructure(self, reminder_args: dict) -> bool:
        print("checkReminderDBStructure")
        model_reminder = {
            "ReminderID": 0,
            "OwnerID": 0,
            "UserIDs": [],
            "GroupIDs": [],
            "ReminderTitle": "Model Reminder",
            "ReminderDescription": "",
            "ReminderStatus": 0,
            "ReminderTag": "",
            "ReminderLocation": "",
            "CreationDate": "",
            "CreationTime": "",
            "CompletionDate": "",
            "CompletionTime": "",
            "ReminderRecurType": 0,
            "FrequencyR": 1,
            "RecurStartDate": "",
            "RecurEndDate": "",
            "RecurNoticeTime": "",
            "RecurDays": [],
            "RecurMonths": [],
            "DateOfMonth": 0,
            "WeekOfMonth": 0,
            "RecurNextReminder": "",
            "recurReminderList": [],
            "RecurringParentID": 0,
            "reminderFlags": []
        }
        for x in reminder_args.copy():
            if x not in model_reminder.keys():
                reminder_args.pop(x)
        for x in model_reminder:
            if x not in reminder_args.keys():
                if x == "OwnerID":
                    continue
                reminder_args[x] = model_reminder[x]
        sort_reminder = reminder_args.copy()
        reminder_args.clear()
        for x in model_reminder:
            reminder_args[x] = sort_reminder[x]
        print(reminder_args)
        if 'OwnerID' not in reminder_args.keys():
            return False
        else:
            return True

    def updateReminder(self, reminder_args: dict) -> dict:
        print("updateReminder")
        reminder = dict(self.rCollection.find_one({"ReminderID": reminder_args["ReminderID"]}))
        update = False

        result = self.rCollection.replace_one({"ReminderID": reminder_args["ReminderID"]}, reminder_args)
        if result.modified_count == 1:
            return reminder_args
        else:
            return {"Error": "updateReminder"}

    def getAllReminders(self):
        """
        Only in use for admins/testing. Gets all reminders in the Reminding.reminder collection
        """
        db = self.connection.Reminding
        return db.reminder.find({})

    def getReminder(self, reminderID):
        """
        Returns a single reminder using the ReminderID
        """
        connection = self.getConnection()
        db = connection.Reminding
        return db.reminder.find({"ReminderID": int(reminderID)})

    def getReminderQuery(self, json_arg):
        """
        Currently in-developement
        """
        if self.checkConnect:
            return_json = dumps(self.rCollection.find(json_arg))
            return return_json
        else:
            return json_arg

    def getRemindersUser(self, userID: int):
        """
        TODO: Add support to also return shared reminders with UserID
        Returns all ACTIVE reminders with a given UserID.
        """
        print("getRemindersUser")
        if self.checkConnect():
            cursor = self.rCollection.find({'OwnerID': userID, 'ReminderStatus': 1}, projection={"_id": False})
            return_args = dumps(cursor)
            print(return_args)
            return return_args

    def getLatestReminderID(self):
        """
        INFO: currently not in use. Can simply use rCollection.count_documents({}) to get the latest index.
              since reminders start at 0, index returns total without 0. no need to increment.
        This method gets a count with the specified owner ID, then returns as the new reminderID.
        """
        print("getLatestReminderID")
        connection = self.getConnection()
        db = connection.Reminding
        # TODO: once we get user loggin replace this with OwnerID passed to us
        return db.reminder.count_documents("{\"OwnerID\":0}")

    def deleteAllReminders(self):
        """
        Should only be used when testing and administrating.
        Deletes all rCollection.
        :return:
        """
        if self.checkConnect():
            result = self.rCollection.delete_many({})
            print(str(result.deleted_count()))
            return result

    def userLogin(self, userRequest: dict, googleUserInfo: iter = iter("test")):
        """
        Handles the user login request
        will first check the json (dict) userRequest we received via https against the model user using checkUserDBStructure
        It will then check if its a new user.
        :param userRequest:
        :param googleUserInfo:
        :return:
        """
        print("userLogin")
        user = {}
        if self.checkConnect():
            if self.checkUserDBStructureUser(userRequest):
                if self.checkNewUser(userRequest, user):
                    return_args = self.addNewUser(userRequest)
                    return return_args
                else:
                    userRequest.update(UserID=user['UserID'])
                    return_args = self.updateUser(userRequest, user)
                    return_args.pop("_id")
                    return return_args
            else:
                return {'UserLogin': "Failed"}

    def checkUserDBStructureUser(self, userRequest: dict) -> bool:
        """
        We use the first document in Mongo - Reminder.users collection to define the structure of the user.
        The body request we received from the application - we are verifying the integrity by retrieving user 0 and seeing
        if the fields in the request are applicable.
        :param userRequest:
        :return:
        """
        print("checkUserDBStructureUser")
        modelUser = {
            'UserID': 1,
            'Email': "Testfile.gmail.com",
            'Password': "password",
            'FirstName': "Jamie",
            'MiddleName': "Mat",
            'LastName': "Ronron",
            'ActivationDate': "2019-11-1",
            'DeactivationDate': "null",
            'UserStatus': 1,
            'GroupIDs': [0, 1]
        }
        for x in userRequest.copy():
            if x not in modelUser.keys():
                userRequest.pop(x)
        for x in modelUser:
            if x not in userRequest.keys():
                userRequest[x] = ""
        sortUser = userRequest.copy()
        userRequest.clear()
        for x in modelUser:
            userRequest[x] = sortUser[x]
        if 'UserID' not in userRequest.keys():
            return False
        else:
            return True

    def checkNewUser(self, userRequest: dict, user: dict) -> bool:
        """
        Checks the database for the userRequest email. if it exists - this returns false, else true.
        :param user: - this is a blank dictionary used to help modify our request
        :param userRequest: - this is the dictionary sent by android app
        :return:
        """
        print("checkNewUser")
        # newUser = False
        finduser = self.uCollection.find_one({'Email': userRequest['Email']})
        if finduser is None:
            userRequest.update(ActivationDate=str(datetime.date.today()))
            return True
        else:
            user.update(dict(finduser))
            return False

    def addNewUser(self, userRequest: dict) -> dict:
        """
        First - counts the user database to get the latest userID. updates the userRequest with that userID.
        Then the userRequest is written to the database.
        :param userRequest: - final request to add to Mongo
        :return:
        """
        print("addNewUser")
        userRequest.update(UserID=self.uCollection.count())
        self.uCollection.insert_one(userRequest)
        userRequest.pop('_id')
        return userRequest

    def updateUser(self, userRequest: dict, user: dict):
        """
        Updates the user in the database if values are different from our userRequest
        :param userRequest: - request sent to us by Android app
        :param user: - dictionary used to help modify userRequest
        :return:
        """
        print("updateUser")
        update = False
        for keys in userRequest.copy():
            if userRequest[keys] == user[keys]:
                userRequest.pop(keys)
            else:
                update = True
        updateRequest = {'$set': userRequest}
        if update:
            result = self.uCollection.update_one({'UserID': user['UserID']}, updateRequest)
            if result.modified_count == 1:
                return self.uCollection.find_one({'UserID', user['UserID']})
            else:
                return user
        else:
            return user

    def checkConnect(self):
        """
        Checks the connection with MongoDB, if we are not connected, attempts to connect to the database.
        :return: - boolean, true for connected, false for not connected.
        """
        try:
            print(self.connection.is_primary)
            databasenames = self.connection.list_database_names()
            if databasenames[0] != 'Reminding':
                self.connection = self.getConnection()
                self.dbase = self.connection.get_database("Reminding")
                self.rCollection = self.dbase.get_collection("reminder")
                print("Connected all via checkConnect")
            elif self.dbase.name != "Reminding":
                self.dbase = self.connection.get_database("Reminding")
                self.rCollection = self.dbase.get_collection("reminder")
                print("Connected database and collection via checkConnect")
            elif self.rCollection.full_name != "Reminding.reminder":
                self.rCollection = self.dbase.get_collection("reminder")
                print("Connected collection via checkConnect")
            elif self.uCollection.full_name != "Reminding.users":
                self.uCollection = self.dbase.get_collection("users")
                print ("Connected user collection via checkConnect")
            else:
                print("Already Connected")
            return True
        except:
            if self.connectionTest:
                print("Error attempting to connect to Mongo")
            return False


