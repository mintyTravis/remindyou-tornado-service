from flask import Flask, render_template, request
from google.auth.transport import requests
from google.oauth2 import id_token

import db

dbase = db.dB()
app = Flask(__name__)


def GetClientID() -> str:
    file = open("configFile", "r")
    clientID = ""
    for x in file:
        if x[0:11] == "client id: ":
            clientID = x[11:].rstrip('\n')
    file.close()
    if clientID == "":
        clientID = "error"
    print(clientID)
    return clientID


def GetClientSecret() -> str:
    file = open("credentials.json", "r")
    clientSecret = ""
    for x in file:
        clientSecret = x
    file.close()
    if clientSecret == "":
        clientSecret = "error"
    return "hTVnwQxRVLy95EEUjXMgFkwi"


@app.route('/')
def root():
    return render_template('index.html', text_to_write="Welcome to REMINDYOU")


@app.route('/remindyou/privatepolicy')
def PrivatePolicy():
    wText = """Remind You - Data Being Accessed
    Contact information - In order to help you create a friends list to send reminders to, we are accessing the contact
    data stored within your phone. These contacts will be displayed when adding friends, and will be stored on a local internal
    file for the app to use to quickly re-create your friends list. No data will be stored external to the phone or sent to,
    or shared with any other entity."""
    return render_template('privatepolicy.html', text_to_write=wText)


@app.route('/reminders/', methods=["POST"])
def AllReminders():
    if request.method == "POST":
        curser = dbase.getAllReminders()
        return_value = ""
        for x in curser:
            return_value += str(x)
        return return_value


@app.route('/reminders/fetch/', methods=["POST"])
def FetchSingleReminder():
    reminder_id = request.json["ReminderID"]
    curser = dbase.getReminder(reminder_id)
    return_value = ""
    for x in curser:
        return_value += str(x)
    return return_value


@app.route('/reminders/fetch/user/', methods=["POST"])
def FetchUserReminders():
    gtoken = request.json[0]["GoogleToken"]
    clientID = GetClientID()
    user_id = request.json[0]["OwnerID"]
    try:
        idinfo = id_token.verify_oauth2_token(gtoken, requests.Request(), clientID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong Issuer')
        return_args = dbase.getRemindersUser(user_id)
    except BaseException as e:
        print(e.args)
        return_args = {"ExceptionError": e.args}
    print(return_args)
    return return_args


@app.route('/reminders/add/', methods=["POST"])
def AddUserReminder():
    gtoken = request.json["GoogleToken"]
    clientID = GetClientID()
    try:
        idinfo = id_token.verify_oauth2_token(gtoken, requests.Request(), clientID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong Issuer')
        return_args = dbase.addReminder(request.json)
    except BaseException as e:
        print(e.args)
        return_args = { "ExceptionError": e.args}
    print(return_args)
    return return_args


"""@app.route('/reminders/testadd', methods=["POST"])
def TestAddReminder():
    try:
        return_args = dbase.addReminder(request.json)
    except BaseException as e:
        print(e.args)
        return_args = {"ExceptionError": e.args}
    print(return_args)"""


@app.route('/reminder/delete_many/', methods=["POST"])
def DeleteAllReminders():
    dbase.deleteAllReminders()
    return


@app.route('/users/login', methods=["POST"])
def UsersLogin():
    # Check google server for valid user. If we have a successful verification. We run dbase.userlogin.
    # If there is no valid verification, we return ValueError failed.
    #
    gtoken = request.json["GoogleToken"]
    clientID = GetClientID()
    return_args: dict

    try:
        idinfo = id_token.verify_oauth2_token(gtoken, requests.Request(), clientID)
        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong Issuer')
        return_args = dbase.userLogin(request.json, idinfo)
    except BaseException as e:
        print(e.args)
        return_args = {
            "ExceptionError": e.args
        }
    print(return_args)
    return return_args


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    # Flask's development server will automatically serve static files in
    # the "static" directory. See:
    # http://flask.pocoo.org/docs/1.0/quickstart/#static-files. Once deployed,
    # App Engine itself will serve those files as configured in app.yaml.
    app.run(host='192.168.1.174', port=5050, debug=True)
